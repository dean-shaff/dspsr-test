"""
get relevant data files from the gDMCP data server
"""
import asyncio
import logging
import argparse
import os
import json

import aiofiles

from dspsr_test import gdmcp_api


def create_parser() -> argparse.ArgumentParser:

    parser = argparse.ArgumentParser("get gDMCP test data")

    parser.add_argument(
        "-d", "--out-dir",
        dest="out_dir", type=str, default=".",
        help="Specify the destination directory for output data")

    parser.add_argument(
        "-v", "--verbose",
        dest="verbose", action="store_true",
        help="Set for high logging verbosity")

    parser.add_argument(
        "--update-dataset-info",
        dest="update_dataset_info", action="store_true",
        help="")

    return parser


async def update_dataset_info(out_dir: str = None):

    api_key = os.getenv("GDMCP_API_KEY", None)
    if api_key is None:
        raise ValueError("Can't proceed without API key")

    file_name = "dataset.info.json"
    if out_dir is None:
        out_dir = os.path.dirname(os.path.abspath(__file__))
        out_dir = os.path.join(out_dir, "test")
    file_path = os.path.join(out_dir, file_name)

    api = gdmcp_api.gDMCP_API(api_key)

    async with api:
        dataset = await api.dataset()
        async with aiofiles.open(file_path, "w") as fd:
            await fd.write(json.dumps(dataset._info))


async def get_data(out_dir: str):

    api_key = os.getenv("GDMCP_API_KEY", None)
    if api_key is None:
        raise ValueError("Can't proceed without API key")

    api = gdmcp_api.gDMCP_API(api_key)

    async with api:
        dataset = await api.dataset()

        coros = [r.download(out_dir) for r in dataset]

        await asyncio.gather(*coros)


if __name__ == "__main__":
    parsed = create_parser().parse_args()
    level = logging.INFO
    if parsed.verbose:
        level = logging.DEBUG

    logging.basicConfig(level=level)

    loop = asyncio.get_event_loop()

    if parsed.update_dataset_info:
        awaitable = update_dataset_info()
    else:
        awaitable = get_data(parsed.out_dir)

    loop.run_until_complete(awaitable)
