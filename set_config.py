import argparse
import os
import shutil

import toml


def create_parser():

    parser = argparse.ArgumentParser(
        description=("Modify some field from the verification "
                     "configuration from the command line"))

    parser.add_argument("-f", "--field", dest="field",
                        required=True, type=str)

    parser.add_argument("-v", "--value", dest="value",
                        required=True, type=str)

    parser.add_argument("--config-file-path", dest="config_file_path",
                        type=str, required=False, default=None)

    return parser


def main():

    parsed = create_parser().parse_args()

    config_file_path = parsed.config_file_path
    if config_file_path is None:
        cur_dir = os.path.dirname(os.path.abspath(__file__))
        config_file_path = os.path.join(cur_dir, "verify.config.toml")

    shutil.copy(config_file_path, config_file_path + ".backup")

    with open(config_file_path, "r") as f:
        config = toml.load(f)

    value_str = f"val={parsed.value}"
    value = toml.loads(value_str)['val']

    sub_fields = parsed.field.split(".")
    config_ = config
    for sub_field in sub_fields[:-1]:
        config_ = config.setdefault(sub_field, {})
    config_[sub_fields[-1]] = value

    with open(config_file_path, "w") as f:
        toml.dump(config, f)


if __name__ == "__main__":
    main()
