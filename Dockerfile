FROM ubuntu:18.04

ENV HOME=/home
ENV DSPSR_TEST_SOURCE=${HOME}/dspsr-test

RUN apt-get update && apt-get -y install curl libbz2-dev libssl-dev libffi-dev zlib1g-dev git gcc build-essential 
RUN curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | bash
ENV PATH="${HOME}/.pyenv/bin:${PATH}"
ENV PYENV_ROOT="${HOME}/.pyenv"
ENV PATH="${PYENV_ROOT}/shims:${PYENV_ROOT}/bin:${PATH}"
RUN pyenv install 3.6.8 
RUN pyenv global 3.6.8
RUN curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | python
ENV PATH="${HOME}/.poetry/bin:${PATH}"

# RUN git clone https://gitlab.com/dean-shaff/dspsr-test.git $DSPSR_SOURCE
COPY . $DSPSR_TEST_SOURCE

WORKDIR $DSPSR_TEST_SOURCE
RUN poetry install

ENTRYPOINT ["poetry", "run", "python"] 

CMD ["get_gDMCP_data.py"]
