import os
import logging
import typing
import asyncio

import partialize

from dspsr_test import util, common


module_logger = logging.getLogger(__name__)

list_of_str = typing.List[str]

options_inputs_type = typing.Union[
    typing.List[list_of_str],
    list_of_str
]


@partialize.partialize
def create_test_cases(config: dict,
                      plot_options: str = "freq+",
                      data_dir: str = "./",
                      output_dir: str = "./") -> list:

    test_case_names = []
    inputs_list = []
    parameters_list = []

    for item in config["params"]:
        test_case_names.append(item["name"])

        file_names = util.make_list(item["files"])
        options = util.make_list(item["options"])

        test_case_data_dir = data_dir
        if "data_dir" in item:
            test_case_data_dir = item["data_dir"]

        inputs_list.append(
            [os.path.join(test_case_data_dir, file_name)
             for file_name in file_names])
        parameters_list.append(options)

    dspsr_bin = config["executable"]

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    def test_case_factory(test_case_name: str,
                          dspsr_bin: str,
                          parameters: options_inputs_type,
                          inputs: options_inputs_type) -> typing.Coroutine:
        """
        generate a test case with a given name.
        This will generate test cases for four difference scenarios:

            - one input (or list of inputs), and one set of ``DSPSR`` command
                line options. If this is the case, then the case simply runs
                ``DSPSR`` on the given data
            - one input (or list of inputs), and two sets of ``DSPSR`` command
                line options. If this occurs, then the case runs ``DSPSR`` on
                the same inputs with different options, and then compares the
                data.
            - two inputs (or list of inputs) and a single set of ``DSPSR``
                command line options. If this occurs then the case runs
                ``DSPSR`` on both data sets using the same options, comparing
                the results using ``psrdiff``.
            - two inputs (or list of inputs) and two sets of ``DSPSR``
                command line options. This will use each set of options for
                list of inputs, comparing the results of each run with
                ``psrdiff``.

        Anytime a pulsar archive is generated, a corresponding psrplot PNG
        is created.

        Args:
            test_case_name: name of test case to generate
            dspsr_bin: The path to the ``DSPSR`` executable
            parameters: A list of command line options to pass to ``DSPSR``
            inputs: A list of file names (not paths) to use as input files
                for ``DSPSR``.
        Returns:
            coroutine: A coroutine that runs the appropriate test case

        """
        if len(inputs) > 2 or len(parameters) > 2:
            raise ValueError(
                ("test_case_factory: cannot create test comparing more "
                 "than two datasets or sets of DSPSR command line options"))

        async def test_case():
            module_logger.debug(
                f"{test_case_name}: dspsr executable: {dspsr_bin}")
            module_logger.debug(f"{test_case_name}: parameters={parameters}")
            module_logger.debug(f"{test_case_name}: inputs={inputs}")

            if len(parameters) == 1 and len(inputs) == 1:
                runner = util.run_dspsr(parameters[0], )

                template = f"{test_case_name}"

                res = await runner(inputs,
                                   dspsr_bin=dspsr_bin,
                                   output_template=template,
                                   output_dir=output_dir)

                await util.run_psrplot(
                    res[0],
                    output_template=template,
                    output_dir=output_dir,
                    plot_options=plot_options)

            else:
                dspsr_coros = []

                iter_template = f"{test_case_name}_{{}}"
                kwds_list = [
                    dict(
                        output_template=iter_template.format(idx),
                        output_dir=output_dir
                    )
                    for idx in range(2)
                ]
                runner = util.run_dspsr(dspsr_bin=dspsr_bin)

                def get_factory(li: list) -> typing.Callable:
                    def _get(idx: int) -> typing.Any:
                        if len(li) == 1:
                            return li[0]
                        return li[idx]
                    return _get

                get_parameters = get_factory(parameters)
                get_inputs = get_factory(inputs)

                for idx in range(2):
                    dspsr_coros.append(
                        runner(get_parameters(idx), get_inputs(idx),
                               **kwds_list[idx]))

                # run dspsr
                res = await asyncio.gather(*dspsr_coros)

                # make plots of archives
                for idx in range(len(res)):
                    await util.run_psrplot(
                            res[idx][0],
                            plot_options=plot_options,
                            **kwds_list[idx])

                # do differencing
                kwds = dict(
                    output_template=f"{test_case_name}_diff",
                    output_dir=output_dir)

                psrdiff_archive = (
                    await util.run_psrdiff(*[r[0] for r in res], **kwds))[0]

                await asyncio.gather(*[
                    util.run_psrtxt(psrdiff_archive, **kwds),
                    util.run_psrplot(
                        psrdiff_archive, plot_options=plot_options, **kwds)
                ])

                module_logger.debug(
                    (f"{test_case_name}: "
                     f"psrdiff_file_path: {psrdiff_archive}"))

        return test_case

    test_cases = []
    for i, method_name in enumerate(test_case_names):
        test_case = test_case_factory(
            method_name, dspsr_bin,
            parameters_list[i], inputs_list[i]
        )
        test_cases.append(test_case)
    return test_cases


if __name__ == "__main__":

    parser = common.create_parser("Run DSPSR")

    parser.add_argument(
        "-p", "--psrplot-options",
        dest="plot_options", default="freq+", action="store")

    parsed = parser.parse_args()

    runner = common.Runner("run_dspsr")
    runner.configure(**vars(parsed))
    runner(
        create_test_cases(
            plot_options=parsed.plot_options,
            data_dir=runner.test_data_dir,
            output_dir=runner.product_dir))
