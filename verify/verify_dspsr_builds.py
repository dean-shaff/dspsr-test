# verify_dspsr_builds.py
"""
verify that for a given set of test cases, two different DSPSR builds produce
the same archives.
"""
import typing
import os
import logging
import asyncio

import partialize
import numpy as np

from dspsr_test import common, run_dspsr, run_psrtxt, load_psrtxt, chain

module_logger = logging.getLogger(__name__)


@partialize.partialize
def create_test_cases(config,
                      data_dir: str = "./",
                      output_dir: str = "./",
                      use_gpu: bool = False,
                      specified_test_names: typing.List[str] = None):
    """
    We need to be able to write to the data dir in which we find ourselves.
    """
    test_case_names = []
    parameters_list = []
    inputs_list = []
    module_logger.debug(f"create_test_cases: specified_test_names={specified_test_names}")
    for item in config["params"]:
        requires_gpu = item.get("gpu", False)
        if requires_gpu and not use_gpu:
            continue
        if specified_test_names is not None:
            if item["name"] not in specified_test_names:
                continue

        test_case_names.append(item["name"])
        parameters_list.append(item["options"])
        inputs_list.append(item["files"])

    os.chdir(data_dir)
    # inputs_list = [os.path.join(data_dir, file_name)
    #                for file_name in inputs_list]

    dspsr_bin0, dspsr_bin1 = config["executables"]

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    module_logger.debug(f"create_test_cases: output_dir={output_dir}")

    def test_case_factory(test_case_name: str,
                          bin0: str,
                          bin1: str,
                          parameters: str,
                          inputs: str):

        async def test_case():

            module_logger.debug(
                f"{test_case_name}: dspsr executable 1: {bin0}")
            module_logger.debug(
                f"{test_case_name}: dspsr executable 2: {bin1}")
            module_logger.debug(f"{test_case_name}: parameters={parameters}")
            module_logger.debug(f"{test_case_name}: inputs={inputs}")

            runner = run_dspsr(parameters, )
            res = await asyncio.gather(
                chain([
                    runner(
                        dspsr_bin=bin0,
                        output_template=f"{test_case_name}_dspsr0",
                        output_dir=output_dir),
                    run_psrtxt(
                        output_template=f"{test_case_name}_dspsr0",
                        output_dir=output_dir),
                    load_psrtxt
                ], inputs),
                chain([
                    runner(
                        dspsr_bin=bin1,
                        output_template=f"{test_case_name}_dspsr1",
                        output_dir=output_dir),
                    run_psrtxt(
                        output_template=f"{test_case_name}_dspsr1",
                        output_dir=output_dir),
                    load_psrtxt
                ], inputs)
            )
            allclose = np.allclose(*res)
            if not allclose:
                raise ValueError((f"{test_case_name} failed: "
                                  "archive files not equalivalent"))

        return test_case

    test_cases = []
    for i, method_name in enumerate(test_case_names):
        test_case = test_case_factory(
            method_name, dspsr_bin0, dspsr_bin1,
            parameters_list[i], inputs_list[i]
        )
        test_cases.append(test_case)
    return test_cases


if __name__ == "__main__":

    parser = common.create_parser("Compare DSPSR builds")

    parsed = parser.parse_args()
    runner = common.Runner("verify_dspsr_builds")
    runner.configure(**vars(parsed))
    runner(create_test_cases(
        data_dir=runner.test_data_dir,
        output_dir=runner.product_dir,
        use_gpu=runner.use_gpu,
        specified_test_names=runner.specified_test_names))
