import asyncio
import shlex
import os
import logging
import functools
import typing
import glob

import numpy as np
import partialize


__all__ = [
    # "test_case_runner",
    "asyncio_test",
    "chain",
    "follow",
    "load_psrtxt",
    "run_psradd",
    "run_psrdiff",
    "run_psrplot",
    "run_psrtxt",
    "run_dspsr",
    "run_dspsr_with_dump",
    "run_psr",
    "make_list"
]


module_logger = logging.getLogger(__name__)


def asyncio_test(coro):
    @functools.wraps(coro)
    def wrapper(*args, **kwargs):
        awaitable = coro(*args, **kwargs)
        loop = asyncio.get_event_loop()
        loop.run_until_complete(awaitable)
    return wrapper


async def _run_coro(coro, *args, **kwargs):
    if hasattr(coro, "__await__"):
        return await coro
    else:
        return await coro(*args, **kwargs)


async def _run_subprocess_cmd(cmd_str, **kwargs):
    proc = await asyncio.create_subprocess_exec(*shlex.split(cmd_str),
                                                **kwargs)
    await proc.communicate()
    return proc


async def chain(coros, a):
    """
    Chain coroutines together, feeding the result from the previous into the
    next. The final result is the result of the last coroutine
    """
    if len(coros) > 1:
        res = await coros[0](a)
        if isinstance(res, tuple):
            res = res[0]
        return await chain(coros[1:], res)
    else:
        return await coros[0](a)


async def follow(coros):
    """
    Chain coroutines together. Return a list of all the results.
    """
    if len(coros) > 1:
        return [await _run_coro(coros[0])] + await follow(coros[1:])
    else:
        return [await _run_coro(coros[0])]


async def load_psrtxt(psrtxt_file: str) -> np.ndarray:
    """
    Load data from psrtxt txt file
    This is only a coroutine such that it can work with chain.
    """
    with open(psrtxt_file, "r") as f:
        contents = f.read()
    arr = []
    for line in contents.split("\n"):
        if line == "":
            continue
        arr.append(np.array([float(a) for a in line.split(" ")]))
    return np.array(arr)


def run_psr(cmd_name, output_format: str = None) -> typing.Coroutine:
    """
    Return a coroutine that runs any arbitrary `psr` command
    """

    async def _run_psr(output_dir: str = "./", output_template: str = None):
        """
        """

    return _run_psr


@partialize.partialize
async def run_psradd(ar_files: str,
                     *,
                     output_dir: str = "./",
                     output_template: str = None) -> typing.Tuple[str]:

    if output_template is None:
        output_template = os.path.splitext(os.path.basename(ar_files[0]))[0]

    output_file_path = os.path.join(output_dir, f"{output_template}.total")
    log_file_path = os.path.join(output_dir, f"psradd_{output_template}.log")

    cmd_str = f"psradd {' '.join(ar_files)} -o {output_file_path}"

    module_logger.debug(f"run_psradd: cmd_str={cmd_str}")

    with open(log_file_path, "w") as log_file:
        proc = await _run_subprocess_cmd(cmd_str,
                                         stdout=log_file,
                                         stderr=log_file)
        if proc.returncode != 0:
            raise RuntimeError(
                f"{cmd_str} failed with exit code {proc.returncode}")
        # else:
        #     await _run_subprocess_cmd(after_cmd_str,
        #                               stdout=log_file,
        #                               stderr=log_file)
    return output_file_path, log_file_path


@partialize.partialize
async def run_psrplot(ar_file: str,
                      *,
                      plot_options: str,
                      output_dir: str = "./",
                      output_template: str = None) -> typing.Tuple[str]:

    if output_template is None:
        output_template = os.path.splitext(os.path.basename(ar_file))[0]

    output_file_path = os.path.join(output_dir, f"{output_template}.png")
    log_file_path = os.path.join(output_dir, f"psrplot_{output_template}.log")

    cmd_str = f"psrplot {ar_file} -p {plot_options} -D /PNG"
    after_cmd_str = f"mv pgplot.png {output_file_path}"

    module_logger.debug(f"run_psrplot: cmd_str={cmd_str}")

    with open(log_file_path, "w") as log_file:
        proc = await _run_subprocess_cmd(cmd_str,
                                         stdout=log_file,
                                         stderr=log_file)
        if proc.returncode != 0:
            raise RuntimeError(
                f"{cmd_str} failed with exit code {proc.returncode}")
        else:
            await _run_subprocess_cmd(after_cmd_str,
                                      stdout=log_file,
                                      stderr=log_file)
    return output_file_path, log_file_path


@partialize.partialize
async def run_psrdiff(ar_file0: str,
                      ar_file1: str,
                      output_dir: str = "./",
                      output_template: str = None) -> typing.Tuple[str]:

    if output_template is None:
        output_template = os.path.splitext(os.path.basename(ar_file0))[0]
    output_file_path = os.path.join(output_dir, f"{output_template}.out")
    log_file_path = os.path.join(output_dir, f"psrdiff_{output_template}.log")
    cmd_str = f"psrdiff {ar_file0} {ar_file1}"
    after_cmd_str = f"mv psrdiff.out {output_file_path}"

    module_logger.debug(f"run_psrdiff: cmd_str={cmd_str}")

    with open(log_file_path, "w") as log_file:
        proc = await _run_subprocess_cmd(cmd_str,
                                         stdout=log_file,
                                         stderr=log_file)
        if proc.returncode != 0:
            raise RuntimeError(
                f"{cmd_str} failed with exit code {proc.returncode}")
        else:
            await _run_subprocess_cmd(after_cmd_str,
                                      stdout=log_file,
                                      stderr=log_file)
    return output_file_path, log_file_path


@partialize.partialize
async def run_psrtxt(input_file: str,
                     output_dir: str = "./",
                     output_template: str = None) -> typing.Tuple[str]:

    if output_template is None:
        output_template = os.path.splitext(os.path.basename(input_file))[0]
    output_file_path = os.path.join(output_dir, f"{output_template}.txt")
    log_file_path = os.path.join(output_dir, f"psrtxt_{output_template}.log")
    cmd_str = f"psrtxt {input_file}"

    module_logger.debug(f"run_psrtxt: cmd_str={cmd_str}")

    with open(log_file_path, "w") as log_file:
        with open(output_file_path, "w") as out_file:
            proc = await _run_subprocess_cmd(cmd_str,
                                             stdout=out_file,
                                             stderr=log_file)
            if proc.returncode != 0:
                raise RuntimeError(
                    f"{cmd_str} failed with exit code {proc.returncode}")

    return output_file_path, log_file_path


@partialize.partialize
async def run_dspsr(parameters: str,
                    inputs: typing.Union[str, typing.List[str]],
                    dspsr_bin: str = "dspsr",
                    multiple_output: bool = False,
                    output_dir: str = "./",
                    output_template: str = None) -> typing.Tuple[str]:

    if hasattr(inputs, "format"):
        inputs = [inputs]

    for file_path in inputs:
        if not os.path.exists(file_path):
            msg = f"{file_path} doesn't exist!"
            print(msg)
            raise RuntimeError(msg)

    if output_template is None:
        output_template = f"{os.path.basename(dspsr_bin)}"

    def get_archive_files() -> typing.Tuple[str, list]:
        if not multiple_output:
            archive_file_names = [f"{output_template}.ar"]
            after_cmd_str = f"mv {archive_file_names[0]} {output_dir}"
        else:
            archive_file_names = glob.glob("*.ar")
            after_cmd_str = f"mv {' '.join(archive_file_names)} {output_dir}"

        return after_cmd_str, archive_file_names

    if not multiple_output:
        cmd_str = (f"{dspsr_bin} "
                   f"{' '.join(inputs)} "
                   f"{parameters} "
                   f"-O {output_template}")
        log_file_path = os.path.join(
            output_dir, f"dspsr_{output_template}.log")
        ar_file_path = os.path.join(
            output_dir, f"{output_template}.ar")

    else:
        cmd_str = (f"{dspsr_bin} "
                   f"{' '.join(inputs)} "
                   f"{parameters} ")
        log_file_path = os.path.join(
            output_dir, f"dspsr_{output_template}.log")

    module_logger.debug(f"run_dspsr: cmd_str={cmd_str}")
    ar_file_path = None
    with open(log_file_path, "w") as log_file:
        proc = await _run_subprocess_cmd(cmd_str,
                                         stdout=log_file,
                                         stderr=log_file)
        if proc.returncode == 0:
            after_cmd_str, ar_file_path = get_archive_files()
            ar_file_path = [os.path.join(output_dir, p) for p in ar_file_path]
            module_logger.debug((f"run_dspsr: after_cmd_str={after_cmd_str},"
                                 f" ar_file_path={ar_file_path}"))
            await _run_subprocess_cmd(after_cmd_str,
                                      stdout=log_file,
                                      stderr=log_file)
        else:
            raise RuntimeError(
                (f"{cmd_str} failed with exit code {proc.returncode}"))
    if ar_file_path is not None:
        if len(ar_file_path) == 1:
            ar_file_path = ar_file_path[0]
    return ar_file_path, log_file_path


@partialize.partialize
async def run_dspsr_with_dump(parameters: str,
                              inputs: typing.Union[str, typing.List[str]],
                              dump_stage: str = "Detection",
                              dspsr_bin: str = "dspsr",
                              output_dir: str = "./",
                              output_template: str = None
                              ) -> typing.Tuple[str]:
    dump_stage = dump_stage.capitalize()
    module_logger.debug(f"run_dspsr_with_dump: dump_stage={dump_stage}")

    if output_template is None:
        output_template = f"{os.path.basename(dspsr_bin)}"

    parameters += f" -dump {dump_stage}"

    dspsr_cmd = run_dspsr(
        parameters, inputs, dspsr_bin, output_dir, output_template)

    dump_file_path = os.path.join(output_dir,
                                  f"pre_{dump_stage}.{output_template}.dump")

    after_cmd_str = f"mv pre_{dump_stage}.dump {dump_file_path}"
    mv_cmd = _run_subprocess_cmd(after_cmd_str)

    res = await follow([dspsr_cmd, mv_cmd])

    return dump_file_path, res[0][0], res[0][1]


def make_list(arg: typing.Union) -> list:

    if hasattr(arg, "__iter__"):
        if hasattr(arg, "format"):
            return [arg]
        return arg

    return [arg]
