# common.py
"""
A set of common tools for configuring and running test cases.
"""
import typing
import asyncio
import os
import logging
import argparse

import tqdm
import toml


__all__ = [
    "create_parser",
    "load_config"
]


def create_parser(description="") -> argparse.ArgumentParser:
    """
    defines a common parser
    """
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument("-v", "--verbose",
                        dest="verbose", action="store_true")

    parser.add_argument("--use-gpu", dest="use_gpu", action="store_true")

    parser.add_argument("-n", "--names",
                        dest="specified_test_names", action="store", nargs="+",
                        help=("Pass a list of specific test names to run"))

    parser.add_argument("-c", "--concurrent",
                        dest="concurrent", action="store_true",
                        help=("Launch all DSPSR processes concurrently. "
                              "Defaults to running commands in sequence "
                              "with a loading bar"))
    return parser


def load_config(file_path: str) -> dict:
    """
    Args:
        file_path (str): path to config file
    Returns:
        dict: configuration dict
    """
    with open(file_path, "r") as f:
        config = toml.load(f)
    return config


class Runner:

    dspsr_test_data_dir_environ_var = "DSPSR_TEST_DATA_DIR"

    def __init__(self, test_name):
        self.test_name = test_name
        paths = self._get_file_paths()
        self._test_config_file_path = paths[1]
        self._product_dir = paths[2]

        self._test_data_dir = None
        self._config = None
        self._concurrent = False
        self._use_gpu = False
        self._specified_test_names = None

    @property
    def product_dir(self):
        return self._product_dir

    @property
    def test_config_file_path(self):
        return self._test_config_file_path

    @property
    def test_data_dir(self):
        return self._test_data_dir

    @property
    def config(self):
        return self._config

    @property
    def specified_test_names(self):
        return self._specified_test_names

    @property
    def use_gpu(self):
        return self._use_gpu

    def _get_file_paths(self) -> typing.Tuple[str]:
        """
        Get a tuple of the relevant file paths for tests
        """
        cur_dir = os.path.dirname(os.path.abspath(__file__))
        test_config_file_path = os.path.join(
            os.path.dirname(cur_dir), "verify.config.toml")
        product_dir = os.path.join(
            os.path.dirname(cur_dir), "products")

        return (cur_dir, test_config_file_path, product_dir)

    def configure(
        self,
        test_config_file_path: str = None,
        product_dir: str = None,
        verbose: bool = False,
        concurrent: bool = False,
        specified_test_names: typing.List[str] = None,
        use_gpu: bool = False,
        **kwargs
    ):
        """
        Any common configuration, like setting log levels or getting directory
        paths
        """
        if test_config_file_path is not None:
            self._test_config_file_path = test_config_file_path

        if product_dir is not None:
            self._product_dir = product_dir

        self._specified_test_names = specified_test_names
        self._use_gpu = use_gpu
        self._concurrent = concurrent

        level = logging.ERROR
        if verbose:
            level = logging.DEBUG

        logging.basicConfig(level=level)
        logging.getLogger("partialize").setLevel(logging.ERROR)

        self._config = load_config(self._test_config_file_path)

        test_data_dir = self._config[self.test_name]["data_dir"]
        if test_data_dir == "":
            test_data_dir = os.getenv(
                self.dspsr_test_data_dir_environ_var, ".")

        self._test_data_dir = test_data_dir

    def __call__(
        self,
        get_test_cases_func: callable
    ):
        """
        Run any tests
        """
        test_config = self._config[self.test_name]

        test_cases = get_test_cases_func(test_config)

        loop = asyncio.get_event_loop()
        if not self._concurrent:
            for idx in tqdm.tqdm(range(len(test_cases))):
                loop.run_until_complete(test_cases[idx]())
        else:
            loop.run_until_complete(
                asyncio.gather(*[case() for case in test_cases]))
