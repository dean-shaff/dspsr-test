import logging
import os
import functools

import aiohttp
import aiofiles

__all__ = [
    "gDMCP_API"
]

module_logger = logging.getLogger(__name__)


def api_context(func):

    @functools.wraps(func)
    async def _inner(self, *args, **kwargs):
        if hasattr(self, "api"):
            api = self.api
        else:
            api = self

        if api._aentered:
            module_logger.debug("api_context._inner: already have api context")
            return await func(self, *args, **kwargs)
        else:
            module_logger.debug("api_context._inner: creating api context")
            async with api:
                return await func(self, *args, **kwargs)

    return _inner


class gDMCP_API_Error(Exception):
    pass


class gDMCP_Asset:
    """
    Some arbitrary asset from a gDMCP API call
    """
    def __init__(self, api, info=None):
        self.api = api
        self._info = info

    def __getitem__(self, item):
        return self._info[item]

    def __contains__(self, item):
        return item in self._info


class gDMCP_Resource(gDMCP_Asset):

    size_threshold = 100 * 10e6  # 100 mb

    @property
    def name(self):
        return self["name"]

    @api_context
    async def download(self, path: str = "./") -> str:
        """
        Download a file from the remote server. If the file is
        larger than the ``size_threshold`` attribute, then asynchronously
        stream the results into the file

        Args:
            path (str): The directory where the downloaded file with end up
        """
        module_logger.debug(f"gDMCP_Resource.download: {path}/{self.name}")

        url = self["url"]
        size = int(self["size"])

        file_path = os.path.join(path, self.name)

        size_threshold = self.__class__.size_threshold

        async def _download():
            async with aiofiles.open(file_path, mode="wb") as fd:
                async with self.api._get(url) as resp:
                    if size > size_threshold:
                        module_logger.debug(
                            ("gDMCP_Resource.download: "
                             "size greater than threshold; "
                             "downloading in chunks"))
                        while True:
                            chunk = await resp.content.read(size_threshold)
                            module_logger.debug(
                                "gDMCP_Resource.download: got chunk")
                            if not chunk:
                                break
                            await fd.write(chunk)
                    else:
                        module_logger.debug(
                            ("gDMCP_Resource.download: "
                             "size less than threshold; "
                             "downloading in single chunk"))
                        await fd.write(await resp.read())

        await _download()

        return file_path


class gDMCP_Dataset(gDMCP_Asset):

    def __iter__(self):
        for item in self["resources"]:
            yield gDMCP_Resource(self.api, item)


class gDMCP_API:

    base_url = "https://data-portal.hpc.swin.edu.au/api/3/{}"

    def __init__(self,
                 api_key,
                 dataset_name="dspsr-test-datasets"):
        self._api_key = api_key
        self.dataset_name = dataset_name
        self._auth_headers = {
            "Authorization": api_key
        }
        self._session = None
        self._aentered = False

    @property
    def api_key(self):
        return self._api_key

    @api_key.setter
    def api_key(self, new_api_key):
        self._api_key = new_api_key
        self._auth_headers["Authorization"] = new_api_key

    @api_context
    async def groups(self):
        """
        get the list of groups that are publically visible
        """
        url = self.base_url.format("action/group_list")
        return await self._get_result(url)

    @api_context
    async def packages(self):
        """
        get a list of all the datasets that are publically visible
        """
        url = self.base_url.format("action/package_list")
        return await self._get_result(url)

    @api_context
    async def tags(self):
        """
        get a list of all the tags that are publically visible
        """
        url = self.base_url.format("action/tag_list")
        return await self._get_result(url)

    @api_context
    async def dataset(self, dataset_name=None):
        if dataset_name is None:
            dataset_name = self.dataset_name
        url = self.base_url.format("action/package_show")
        result = await self._get_result(url, params={"id": dataset_name})
        return gDMCP_Dataset(self, result)

    async def _get_result(self, *args, **kwargs):
        # async with self._get(*args, **kwargs) as response:
        #     result = await response.json()
        result = await self._get_json(*args, **kwargs)
        if result["success"]:
            return result["result"]
        else:
            raise gDMCP_API_Error("Couldn't make request!")

    async def _get_json(self, url, session=None, **kwargs):
        if session is None:
            session = self._session

        async with session.get(url, **kwargs) as response:
            return await response.json()

    def _get(self, url, session=None, **kwargs):
        module_logger.debug(f"gDMCP_API._get: url={url}, kwargs={kwargs}")
        if session is None:
            session = self._session

        return session.get(url, **kwargs)

    async def __aenter__(self, *args, **kwargs):
        module_logger.debug("gDMCP_API.__aenter__")
        self._session = aiohttp.ClientSession(
            connector=aiohttp.TCPConnector(ssl=False),
            headers=self._auth_headers
        )
        await self._session.__aenter__(*args, **kwargs)
        self._aentered = True
        return self

    async def __aexit__(self, *args, **kwargs):
        module_logger.debug("gDMCP_API.__aexit__")
        res = await self._session.__aexit__(*args, **kwargs)
        self._aentered = False
        return res
