## DSPSR tests

This repo contains several DSPSR verification tests.

### Installing

`poetry install`

### Configuring

Update `verify.config.toml` with different test cases and DSPSR builds.

#### Configuring `verify_dspsr_builds`

Each entry in the `verify_dspsr_builds.params` section of the verfiy.config.toml
configuration file has the following contents:

```TOML
[
  <<test case identifier>>,
  <<DSPSR command line options>>
  <<test file name>>
]
```

For example:

```TOML
[
  "default", # the name of the test case
  "-E pulsar.par -P t2pred.dat -F 128:D -T 1", # parameters that are passed to DSPSR
  "1644-4559.dada" # test file name
],
```

Adding more entries will create more test cases.

#### Configuring `run_dspsr`

The `run_dspsr` verification tool can be used to automate a few scenarios:

1. Running DSPSR with a given set of command line options against a given
file. These tests don't check for correctness, rather they just determine if
DSPSR will run at all.
2. Running DSPSR with two sets of command line options, using the same data
file.
3. Running DSPSR with one sets of command line options, using two different
data files.
4. Running DSPSR with two sets of command line options, using two data files.

For cases 2-4, the test cases automatically use `psrdiff` to compare the output
archive of each DSPSR run.

The following is an example of snippet of a possible `verify.config.toml` file
that will create a test case for each of the preceding scenarios:

```toml
# scenario 1
[[run_dspsr.params]]
name = "F_128D"
options = ["-F 128:D -T 0.05"]
files = ["1024/2018-09-01-09:52:55_0000000000000000.000000.dada"]

# scenario 2
[[run_dspsr.params]]
name = "sk_1024_rfi"
options = [
  "-F 128:D -skz -skzm 128 -skzs 3 -U 10240 -T 0.05",
  "-F 128:D -U 10240 -T 0.05"
]
files = ["1024/2018-09-01-09:52:55_0000000000000000.000000.dada"]

# scenario 3
[[run_dspsr.params]]
name = "sk_1024"
options = [
  "-F 128:D -U 10240 -T 0.05"
]
files = [
  "1024/2018-09-01-09:52:55_0000000000000000.000000.dada",
  "1024/2018-09-01-09:52:55_0000010267656192.000000.dada"
]

# scenario 4
[[run_dspsr.params]]
name = "sk_1024_rfi_no_rfi"
options = [
  "-F 128:D -skz -skzm 128 -skzs 3 -U 10240 -T 0.05",
  "-F 128:D -U 10240 -T 0.05"
]
files = [
  "1024/2018-09-01-09:52:55_0000000000000000.000000.dada",
  "1024/2018-09-01-09:52:55_0000010267656192.000000.dada"
]
```

### Running

To test DSPSR builds against one another:

```
poetry run python -m verify.verify_dspsr_builds
```

To run DSPSR with varying parameters on the same dataset (this does not
check to see if any output is _correct_):

```
poetry run python -m verify.run_dspsr
```

Note that using the `-c` command line option with `run_dspsr` may lead to
undefined behavior.

### Downloading gDMCP data


If `dspsr-test` is installed with poetry, simply run

```
poetry run python get_gDMCP_data.py -d ./data
```

This assumes that the GDMCP_API_KEY environment variable has been set.

```
docker run -v ./data:/home/data -e GDMCP_API_KEY="<gdmcp-api-key>" -t dshaff/dspsr-test:latest get_gDMCP_data.py -d /home/data/
```


### Testing

```
poetry run python -m test.test_util
```
