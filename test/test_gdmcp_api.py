import os
import logging
import unittest
import json
import functools
import asyncio

from dspsr_test import util, gdmcp_api

cur_dir = os.path.dirname(os.path.abspath(__file__))
dataset_info_file_path = os.path.join(cur_dir, "dataset.info.json")


def api_key_test_case_factory():

    class TestCase(unittest.TestCase):

        @classmethod
        def setUpClass(cls):
            api_key = os.getenv("GDMCP_API_KEY", None)
            if api_key is None:
                raise unittest.SkipTest(
                    "Can't run API tests without an API key!")
            cls.api_key = api_key

        def setUp(self):
            self.api = gdmcp_api.gDMCP_API(self.__class__.api_key)

    return TestCase


def test_context(func):

    @functools.wraps(func)
    async def _test_method(self):
        async with self.api:
            await func(self)
        await func(self)

    return _test_method


class TestGDMCP_Resource(api_key_test_case_factory()):

    @classmethod
    def setUpClass(cls):
        super(TestGDMCP_Resource, cls).setUpClass()
        with open(dataset_info_file_path, "r") as fd:
            dataset_info = json.load(fd)
        cls.resources_info = dataset_info["resources"]
        cls.downloaded_files = []

    @util.asyncio_test
    async def test_download(self):

        small_file_name = "pulsar.par"  # a few bytes
        big_file_name = "1644-4559.dada"  # roughly 500 mb

        resources = []
        for file_name in [small_file_name, big_file_name]:
            for item in self.resources_info:
                if item["name"] == file_name:
                    resource = gdmcp_api.gDMCP_Resource(self.api, item)
                    resources.append(resource)
                    break

        async with self.api:
            coros = [r.download(cur_dir) for r in resources]
            file_paths = await asyncio.gather(*coros)
            for idx in range(len(file_paths)):
                resource = resources[idx]
                file_path = file_paths[idx]
                expected_size = int(resource["size"])
                self.assertTrue(os.path.getsize(file_path) == expected_size)
                self.__class__.downloaded_files.append(file_path)

        # now try without the api context
        file_path = await resources[0].download(cur_dir)

    @classmethod
    def tearDownClass(cls):
        for file_path in cls.downloaded_files:
            if os.path.exists(file_path):
                os.remove(file_path)


class TestGDMCP_Dataset(api_key_test_case_factory()):

    @classmethod
    def setUpClass(cls):
        super(TestGDMCP_Dataset, cls).setUpClass()
        with open(dataset_info_file_path, "r") as fd:
            dataset_info = json.load(fd)
        cls.dataset_info = dataset_info

    def setUp(self):
        super(TestGDMCP_Dataset, self).setUp()
        self.dataset = gdmcp_api.gDMCP_Dataset(
            self.api, self.dataset_info)

    def test_iter(self):
        resources = list(self.dataset)
        self.assertTrue(isinstance(resources[0], gdmcp_api.gDMCP_Resource))
        expected_file_names = [
            "pulsar.par",
            "1644-4559.dada",
            "t2pred.dat"
        ]

        file_names = [res.name for res in self.dataset]

        for fn in expected_file_names:
            self.assertTrue(fn in file_names)

    def test_getitem(self):
        expected_val = "dataset"
        self.assertTrue(self.dataset["type"] == expected_val)

    def test_contains(self):
        self.assertTrue("metadata_modified" in self.dataset)


class TestGDMCP_API(api_key_test_case_factory()):

    @util.asyncio_test
    async def test__get(self):
        url = self.api.base_url.format("")
        async with self.api:
            async with self.api._get(url) as resp:
                data = await resp.json()
            self.assertTrue("version" in data)

    @util.asyncio_test
    async def test__get_json(self):
        url = self.api.base_url.format("")
        async with self.api:
            data = await self.api._get_json(url)
            self.assertTrue("version" in data)

    @util.asyncio_test
    @test_context
    async def test_groups(self):
        resp = await self.api.groups()
        self.assertTrue(hasattr(resp, "append"))

    @util.asyncio_test
    @test_context
    async def test_packages(self):
        resp = await self.api.packages()
        self.assertTrue(hasattr(resp, "append"))

    @util.asyncio_test
    @test_context
    async def test_tags(self):
        resp = await self.api.tags()
        self.assertTrue(hasattr(resp, "append"))

    @util.asyncio_test
    @test_context
    async def test_dataset(self):
        dataset = await self.api.dataset()
        self.assertTrue("maintainer" in dataset)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    unittest.main()
