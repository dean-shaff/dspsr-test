import os
import unittest
import logging

import toml

from dspsr_test import (
    asyncio_test,
    chain,
    follow,
    run_dspsr,
    run_dspsr_with_dump
)

cur_dir = os.path.dirname(os.path.abspath(__file__))
config_file_path = os.path.join(cur_dir, "test.config.toml")

with open(config_file_path, "r") as f:
    config = toml.load(f)

test_data_file_path = config["test_data_file_path"]
dspsr_bin = config["dspsr_bin"]
default_dspsr_args = config["default_args"]


class TestUtil(unittest.TestCase):

    @asyncio_test
    async def test_chain(self):

        async def coro1(a):
            return a

        self.assertTrue(await chain([coro1, coro1, coro1], "hello") == "hello")

    @asyncio_test
    async def test_follow(self):

        async def coro1():
            return "hello"

        expected_result = ["hello", "hello", "hello"]
        actual_result = await follow([coro1, coro1, coro1])
        self.assertTrue(expected_result == actual_result)

        actual_result = await follow([coro1(), coro1(), coro1()])
        self.assertTrue(expected_result == actual_result)


def test_dspsr_factory(test_data_file_path):

    dspsr_args = default_dspsr_args + " -F 8:D -V"

    @asyncio_test
    async def test_run_dspsr(self):

        coro = run_dspsr(
            dspsr_args, test_data_file_path,
            dspsr_bin=dspsr_bin, output_dir=cur_dir)
        file_paths = await coro
        for file_path in file_paths:
            self.assertTrue(os.path.exists(file_path))
            os.remove(file_path)

        coro = run_dspsr(
            dspsr_args, test_data_file_path + "foo",
            dspsr_bin=dspsr_bin, output_dir=cur_dir)

        with self.assertRaises(RuntimeError):
            await coro

    @asyncio_test
    async def test_run_dspsr_with_dump(self):
        coro = run_dspsr_with_dump(
            dspsr_args, test_data_file_path,
            dump_stage="Detection",
            dspsr_bin=dspsr_bin,
            output_dir=cur_dir)
        file_paths = await coro
        for file_path in file_paths:
            self.assertTrue(os.path.exists(file_path))
            os.remove(file_path)

    return test_run_dspsr, test_run_dspsr_with_dump


if os.path.exists(test_data_file_path) and os.path.exists(dspsr_bin):
    methods = test_dspsr_factory(test_data_file_path)
    for method in methods:
        setattr(TestUtil, method.__name__, method)


if __name__ == "__main__":
    logging.basicConfig(level=logging.ERROR)
    unittest.main()
