import asyncio
import subprocess
import time


def run_subprocess():
    def _run():
        return subprocess.Popen(["sleep", "3"])
    procs = [_run() for i in range(2)]
    for proc in procs:
        proc.communicate()


# t0 = time.time()
# run_subprocess()
# print(f"delta={time.time() - t0}")


async def run():
    proc = await asyncio.create_subprocess_exec("sleep", "10")
    await proc.communicate()

loop = asyncio.get_event_loop()
task = asyncio.gather(*[run() for i in range(4)])
t0 = time.time()
loop.run_until_complete(task)
print(f"delta={time.time() - t0}")
